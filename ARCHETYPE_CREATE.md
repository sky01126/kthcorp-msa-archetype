# Archetype 프로젝트
* **Archetype 프로젝트 생성 후 다른 저장소가 없는 경우에는 아래와 같이 로컬에 바로 등록한다.**

```bash
mvn clean
mvn -X archetype:create-from-project -Darchetype.properties=archetype.properties

# Git Ignore 파일 복사.
cp .gitignore target/generated-sources/archetype/target/classes/archetype-resources/

# Archetype 프로젝트를 로컬 Repository에 등록
cd target/generated-sources/archetype
mvn install
```

* **Archetype을 이용해서 프로젝트 생성.**

>"archetypeCatalog=local" 옵션은 로컬 Repository에서 생성을 지원.

```bash
mvn archetype:generate                                              \
    -DarchetypeCatalog=local                                        \
    -DarchetypeGroupId=com.kthcorp.archetype                        \
    -DarchetypeArtifactId=kthcorp-base-archetype                    \
    -DarchetypeVersion=1.0.2.RELEASE                                \
    -DgroupId=com.kthcorp                                           \
    -Dversion=1.0.0.RELEASE                                         \
    -DartifactId=employees                                          \
    -DinteractiveMode=false
```
