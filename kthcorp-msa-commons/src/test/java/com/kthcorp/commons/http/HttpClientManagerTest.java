package com.kthcorp.commons.http;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kthcorp.commons.lang.http.HttpClientManager;

public class HttpClientManagerTest {

	private static final Logger log = LoggerFactory.getLogger(HttpClientManagerTest.class);

	private String url = "https://www.google.co.kr";

	@Test
	public void test() throws IOException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
		CloseableHttpResponse httpResponse = null;
		HttpClientManager httpManager = null;
		try {
			httpManager = HttpClientManager.get();
			httpManager.setConnectTimeout(2 * 1000);
			httpManager.setSocketTimeout(5 * 1000);

			httpResponse = httpManager.execute(url);
			log.debug(EntityUtils.toString(httpResponse.getEntity()));
		} finally {
			if (httpResponse != null) {
				try {
					httpResponse.close();
				} catch (IOException e) {
					log.warn(e.getMessage(), e);
				}
			}
			if (httpManager != null) {
				httpManager.close();
			}
		}
	}

}
