/*
 * Copyright(C) 2017 KT Hitel Co., Ltd. all rights reserved.
 *
 * This is a proprietary software of KTH corp, and you may not use this file except in
 * compliance with license with license agreement with KTH corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of KTH corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 */

package com.kthcorp.archetype.commons.persistence.dao;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.RowBounds;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import com.kthcorp.commons.lang.AbstractProperty;
import com.kthcorp.commons.lang.ArrayUtils;

/**
 * 추상화된 기본 Data Access Object
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @author <a href="mailto:gaeul.lee@kt.com"><b>이가을</b></a>
 * @version 1.0.0
 * @since 7.0
 */
@Component
public abstract class AbstractMybatisDao extends AbstractProperty {

	private static final Logger log = LoggerFactory.getLogger(AbstractMybatisDao.class);

	/**
	 * Master Database SQL Session Template
	 */
	@Resource(name = "sqlSessionTemplateMaster")
	private SqlSessionTemplate sqlSessionTemplateMaster;

	/**
	 * Slave Database SQL Session Template
	 */
	@Resource(name = "sqlSessionTemplateSlave")
	private SqlSessionTemplate sqlSessionTemplateSlave;

	public AbstractMybatisDao() {
		// ignore...
	}

	/**
	 * Getter Sql session Template
	 *
	 * @param isSlave 조회를 Slave DB에서 진행 할 것인지 설정.
	 * @return the sqlSessionTemplate
	 */
	protected SqlSessionTemplate getSqlSessionTemplate(boolean isSlave) {
		if (isSlave) {
			boolean isReadOnlay = TransactionSynchronizationManager.isCurrentTransactionReadOnly();
			if (log.isDebugEnabled()) {
				log.debug("[{}] Current Transaction Read Only: {} ", isReadOnlay ? "SLAVE DB" : "MASTER DB",
						isReadOnlay);
			}
			if (this.sqlSessionTemplateSlave == null) {
				this.sqlSessionTemplateSlave = this.sqlSessionTemplateMaster;
			}
			return isReadOnlay ? sqlSessionTemplateSlave : this.sqlSessionTemplateMaster;
		} else {
			return this.sqlSessionTemplateMaster;
		}
	}

	/**
	 * Select One without parameter
	 *
	 * @param <T> the returned object type
	 * @param statement Unique identifier matching the statement to execute.
	 * @return T the returned object type
	 */
	protected <T> T selectOne(String statement) {
		List<T> list = getSqlSessionTemplate(true).selectList(statement);
		return ArrayUtils.isNotEmpty(list) ? list.get(0) : null;
	}

	/**
	 * Select One without parameter
	 *
	 * @param <T> the returned object type
	 * @param sqlSessionTemplate the sql session template.
	 * @param statement Unique identifier matching the statement to execute.
	 * @return T the returned object type
	 */
	protected <T> T selectOne(SqlSessionTemplate sqlSessionTemplate, String statement) {
		List<T> list = sqlSessionTemplate.selectList(statement);
		return ArrayUtils.isNotEmpty(list) ? list.get(0) : null;
	}

	/**
	 * Select One without parameter
	 *
	 * @param <T> the returned object type
	 * @param statement Unique identifier matching the statement to execute.
	 * @return T the returned object type
	 */
	protected <T> T selectOneInMaster(String statement) {
		List<T> list = getSqlSessionTemplate(false).selectList(statement);
		return ArrayUtils.isNotEmpty(list) ? list.get(0) : null;
	}

	/**
	 * Select One without parameter
	 *
	 * @param <T> the returned object type
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @return T the returned object type
	 */
	protected <T> T selectOne(String statement, Object parameter) {
		List<T> list = getSqlSessionTemplate(true).selectList(statement, parameter);
		return ArrayUtils.isNotEmpty(list) ? list.get(0) : null;
	}

	/**
	 * Select One without parameter
	 *
	 * @param <T> the returned object type
	 * @param sqlSessionTemplate the sql session template.
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @return T the returned object type
	 */
	protected <T> T selectOne(SqlSessionTemplate sqlSessionTemplate, String statement, Object parameter) {
		List<T> list = sqlSessionTemplate.selectList(statement, parameter);
		return ArrayUtils.isNotEmpty(list) ? list.get(0) : null;
	}

	/**
	 * Select One without parameter
	 *
	 * @param <T> the returned object type
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @return T the returned object type
	 */
	protected <T> T selectOneInMaster(String statement, Object parameter) {
		List<T> list = getSqlSessionTemplate(false).selectList(statement, parameter);
		return ArrayUtils.isNotEmpty(list) ? list.get(0) : null;
	}

	/**
	 * Select One without parameter
	 *
	 * @param <T> the returned object type
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @param rowBounds Row Bounds
	 * @return T the model
	 */
	protected <T> T selectOne(String statement, Object parameter, RowBounds rowBounds) {
		List<T> list = getSqlSessionTemplate(true).selectList(statement, parameter, rowBounds);
		return ArrayUtils.isNotEmpty(list) ? list.get(0) : null;
	}

	/**
	 * Select One without parameter
	 *
	 * @param <T> the returned object type
	 * @param sqlSessionTemplate the sql session template.
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @param rowBounds Row Bounds
	 * @return T the returned object type
	 */
	protected <T> T selectOne(SqlSessionTemplate sqlSessionTemplate, String statement, Object parameter,
			RowBounds rowBounds) {
		List<T> list = sqlSessionTemplate.selectList(statement, parameter, rowBounds);
		return ArrayUtils.isNotEmpty(list) ? list.get(0) : null;
	}

	/**
	 * Select One without parameter
	 *
	 * @param <T> the returned object type
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @param rowBounds Row Bounds
	 * @return T the returned object type
	 */
	protected <T> T selectOneInMaster(String statement, Object parameter, RowBounds rowBounds) {
		List<T> list = getSqlSessionTemplate(false).selectList(statement, parameter, rowBounds);
		return ArrayUtils.isNotEmpty(list) ? list.get(0) : null;
	}

	/**
	 * Select List without parameter
	 *
	 * @param <E> the returned list element type
	 * @param statement Unique identifier matching the statement to execute.
	 * @return List of mapped object of mapped object
	 */
	protected <E> List<E> selectList(String statement) {
		List<E> list = getSqlSessionTemplate(true).selectList(statement);
		return ArrayUtils.isNotEmpty(list) ? list : null;
	}

	/**
	 * Select List without parameter
	 *
	 * @param <E> the returned list element type
	 * @param sqlSessionTemplate the sql session template.
	 * @param statement Unique identifier matching the statement to execute.
	 * @return List of mapped object
	 */
	protected <E> List<E> selectList(SqlSessionTemplate sqlSessionTemplate, String statement) {
		List<E> list = sqlSessionTemplate.selectList(statement);
		return ArrayUtils.isNotEmpty(list) ? list : null;
	}

	/**
	 * Select List without parameter
	 *
	 * @param <E> the returned list element type
	 * @param statement Unique identifier matching the statement to execute.
	 * @return List of mapped object
	 */
	protected <E> List<E> selectListInMaster(String statement) {
		List<E> list = getSqlSessionTemplate(false).selectList(statement);
		return ArrayUtils.isNotEmpty(list) ? list : null;
	}

	/**
	 * Select List with parameter
	 *
	 * @param <E> the returned list element type
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @return List of mapped object
	 */
	protected <E> List<E> selectList(String statement, Object parameter) {
		List<E> list = getSqlSessionTemplate(true).selectList(statement, parameter);
		return ArrayUtils.isNotEmpty(list) ? list : null;
	}

	/**
	 * Select List with parameter
	 *
	 * @param <E> the returned list element type
	 * @param sqlSessionTemplate the sql session template.
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @return List of mapped object
	 */
	protected <E> List<E> selectList(SqlSessionTemplate sqlSessionTemplate, String statement, Object parameter) {
		List<E> list = getSqlSessionTemplate(true).selectList(statement, parameter);
		return ArrayUtils.isNotEmpty(list) ? list : null;
	}

	/**
	 * Select List with parameter
	 *
	 * @param <E> the returned list element type
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @return List of mapped object
	 */
	protected <E> List<E> selectListInMaster(String statement, Object parameter) {
		List<E> list = getSqlSessionTemplate(false).selectList(statement, parameter);
		return ArrayUtils.isNotEmpty(list) ? list : null;
	}

	/**
	 * Select List with parameter
	 *
	 * @param <E> the returned list element type
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @param rowBounds Row Bounds
	 * @return List of mapped object
	 */
	protected <E> List<E> selectList(String statement, Object parameter, RowBounds rowBounds) {
		List<E> list = getSqlSessionTemplate(true).selectList(statement, parameter, rowBounds);
		return ArrayUtils.isNotEmpty(list) ? list : null;
	}

	/**
	 * Select List with parameter
	 *
	 * @param <E> the returned list element type
	 * @param sqlSessionTemplate the sql session template.
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @param rowBounds Row Bounds
	 * @return List of mapped object
	 */
	protected <E> List<E> selectList(SqlSessionTemplate sqlSessionTemplate, String statement, Object parameter,
			RowBounds rowBounds) {
		List<E> list = sqlSessionTemplate.selectList(statement, parameter, rowBounds);
		return ArrayUtils.isNotEmpty(list) ? list : null;
	}

	/**
	 * Select List with parameter
	 *
	 * @param <E> the returned list element type
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @param rowBounds Row Bounds
	 * @return List of mapped object
	 */
	protected <E> List<E> selectListInMaster(String statement, Object parameter, RowBounds rowBounds) {
		List<E> list = getSqlSessionTemplate(false).selectList(statement, parameter, rowBounds);
		return ArrayUtils.isNotEmpty(list) ? list : null;
	}

	/**
	 * Insert with parameter
	 *
	 * @param statement Unique identifier matching the statement to execute.
	 * @return int The number of rows affected by the insert.
	 */
	protected int insert(String statement) {
		return getSqlSessionTemplate(false).insert(statement);
	}

	/**
	 * Insert with parameter
	 *
	 * @param sqlSessionTemplate the sql session template.
	 * @param statement Unique identifier matching the statement to execute.
	 * @return int The number of rows affected by the insert.
	 */
	protected int insert(SqlSessionTemplate sqlSessionTemplate, String statement) {
		return sqlSessionTemplate.insert(statement);
	}

	/**
	 * Insert with parameter
	 *
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @return int The number of rows affected by the insert.
	 */
	protected int insert(String statement, Object parameter) {
		return getSqlSessionTemplate(false).insert(statement, parameter);
	}

	/**
	 * Insert with parameter
	 *
	 * @param sqlSessionTemplate the sql session template.
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @return int The number of rows affected by the insert.
	 */
	protected int insert(SqlSessionTemplate sqlSessionTemplate, String statement, Object parameter) {
		return sqlSessionTemplate.insert(statement, parameter);
	}

	/**
	 * Insert or Update with parameter
	 *
	 * @param statement Unique identifier matching the statement to execute.
	 * @return int The number of rows affected by the update.
	 */
	protected int upsert(String statement) {
		return getSqlSessionTemplate(false).update(statement);
	}

	/**
	 * Insert or Update with parameter
	 *
	 * @param sqlSessionTemplate the sql session template.
	 * @param statement Unique identifier matching the statement to execute.
	 * @return int The number of rows affected by the update.
	 */
	protected int upsert(SqlSessionTemplate sqlSessionTemplate, String statement) {
		return sqlSessionTemplate.update(statement);
	}

	/**
	 * Insert or Update with parameter
	 *
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @return int The number of rows affected by the update.
	 */
	protected int upsert(String statement, Object parameter) {
		return getSqlSessionTemplate(false).update(statement, parameter);
	}

	/**
	 * Insert or Update with parameter
	 *
	 * @param sqlSessionTemplate the sql session template.
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @return int The number of rows affected by the update.
	 */
	protected int upsert(SqlSessionTemplate sqlSessionTemplate, String statement, Object parameter) {
		return sqlSessionTemplate.update(statement, parameter);
	}

	/**
	 * Update with parameter
	 *
	 * @param statement Unique identifier matching the statement to execute.
	 * @return int The number of rows affected by the update.
	 */
	protected int update(String statement) {
		return getSqlSessionTemplate(false).update(statement);
	}

	/**
	 * Update with parameter
	 *
	 * @param sqlSessionTemplate the sql session template.
	 * @param statement Unique identifier matching the statement to execute.
	 * @return int The number of rows affected by the update.
	 */
	protected int update(SqlSessionTemplate sqlSessionTemplate, String statement) {
		return sqlSessionTemplate.update(statement);
	}

	/**
	 * Update with parameter
	 *
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @return int The number of rows affected by the update.
	 */
	protected int update(String statement, Object parameter) {
		return getSqlSessionTemplate(false).update(statement, parameter);
	}

	/**
	 * Update with parameter
	 *
	 * @param sqlSessionTemplate the sql session template.
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @return int The number of rows affected by the update.
	 */
	protected int update(SqlSessionTemplate sqlSessionTemplate, String statement, Object parameter) {
		return sqlSessionTemplate.update(statement, parameter);
	}

	/**
	 * Delete without parameter
	 *
	 * @param statement Unique identifier matching the statement to execute.
	 * @return int The number of rows affected by the delete.
	 */
	protected int delete(String statement) {
		return getSqlSessionTemplate(false).delete(statement);
	}

	/**
	 * Delete without parameter
	 *
	 * @param sqlSessionTemplate the sql session template.
	 * @param statement Unique identifier matching the statement to execute.
	 * @return int The number of rows affected by the delete.
	 */
	protected int delete(SqlSessionTemplate sqlSessionTemplate, String statement) {
		return sqlSessionTemplate.delete(statement);
	}

	/**
	 * Delete with parameter
	 *
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @return int The number of rows affected by the delete.
	 */
	protected int delete(String statement, Object parameter) {
		return getSqlSessionTemplate(false).delete(statement, parameter);
	}

	/**
	 * Delete with parameter
	 *
	 * @param sqlSessionTemplate the sql session template.
	 * @param statement Unique identifier matching the statement to execute.
	 * @param parameter A parameter object to pass to the statement.
	 * @return int The number of rows affected by the delete.
	 */
	protected int delete(SqlSessionTemplate sqlSessionTemplate, String statement, Object parameter) {
		return sqlSessionTemplate.delete(statement, parameter);
	}

}
