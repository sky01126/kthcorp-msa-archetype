/*
 * Copyright ⓒ [2017] KTH corp.All rights reserved.
 *
 * This is a proprietary software of KTH corp, and you may not use this file except in
 * compliance with license with license agreement with KTH corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of KTH corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 */

package com.kthcorp.archetype.commons.config;

import com.kthcorp.commons.config.CommonJsonName;

/**
 * Json Name 설정
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @version 1.0.0
 * @see
 * @since 7.0
 */
@SuppressWarnings("all")
public class JsonName extends CommonJsonName {

	/** 사용자 아이디. */
	public static final String USER_ID = "userid";

	/** 사용자 비밀번호 */
	public static final String USER_PASSWORD = "passwd";

	/** 사용자명. */
	public static final String USER_NAME = "user_name";

	/** 데이터. */
	public static final String DATA = "data";

	/** 리스트. */
	public static final String LIST = "list";

	private JsonName() {
		// ignore...
	}

}
