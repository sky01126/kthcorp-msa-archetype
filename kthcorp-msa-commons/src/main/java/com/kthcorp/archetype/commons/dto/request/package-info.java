/**
 * 프로젝트 공통 Request DTO(Data Transfer Object) 클래스 제공.
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @version 1.0.0
 * @since 7.0
 */
package com.kthcorp.archetype.commons.dto.request;
