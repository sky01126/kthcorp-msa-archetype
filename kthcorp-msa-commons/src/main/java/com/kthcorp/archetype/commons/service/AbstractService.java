/*
 * Copyright ⓒ [2017] KTH corp.All rights reserved.
 *
 * This is a proprietary software of KTH corp, and you may not use this file except in
 * compliance with license with license agreement with KTH corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of KTH corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 */

package com.kthcorp.archetype.commons.service;

import org.springframework.stereotype.Component;

import com.kthcorp.commons.lang.AbstractProperty;

/**
 * Abstract Service
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @version 1.0.0
 * @see AbstractProperty
 * @since 7.0
 */
@Component
public abstract class AbstractService extends AbstractProperty {

	public AbstractService() {
		// ignore...
	}

}
