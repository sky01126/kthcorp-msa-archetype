/*
 * Copyright ⓒ [2017] KTH corp.All rights reserved.
 *
 * This is a proprietary software of KTH corp, and you may not use this file except in
 * compliance with license with license agreement with KTH corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of KTH corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 */

package com.kthcorp.archetype.commons;

import com.kthcorp.commons.jdbc.HikariDataSourceConfiguration;

/**
 * Abstract DataSource Configuration
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @version 1.0.0
 * @see HikariDataSourceConfiguration
 * @since 7.0
 */
public abstract class AbstractDataSourceConfiguration extends HikariDataSourceConfiguration {

	/**
	 * Slave DataSource Configuration 생성.
	 */
	public AbstractDataSourceConfiguration() {
		super.setConfigFile("com/kthcorp/archetype/commons/persistence/mybatis-config.xml");
		super.setMapperPath("classpath*:com/kthcorp/archetype/**/persistence/mapper/**/*.xml");
		super.add("com.kthcorp.archetype.commons.dto.request");
		super.add("com.kthcorp.archetype.commons.dto.response");
		super.add("com.kthcorp.archetype.commons.persistence.model");
		super.add("com.kthcorp.archetype.security.dto.request");
		super.add("com.kthcorp.archetype.security.dto.response");
		super.add("com.kthcorp.archetype.security.persistence.model");
		super.add("com.kthcorp.archetype.dto.request");
		super.add("com.kthcorp.archetype.dto.response");
		super.add("com.kthcorp.archetype.persistence.model");
	}

}
