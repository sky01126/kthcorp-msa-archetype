/*
 * Copyright ⓒ [2017] KTH corp.All rights reserved.
 *
 * This is a proprietary software of KTH corp, and you may not use this file except in
 * compliance with license with license agreement with KTH corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of KTH corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 */

package com.kthcorp.archetype;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.common.collect.Lists;
import com.kthcorp.commons.properties.AppInfoProperty;
import com.kthcorp.commons.properties.ResponseStatusProperty;

import io.swagger.annotations.ApiOperation;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Swagger UI를 이용해서 API 테스트 패이지 생성 설정
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @version 1.0.0
 * @since 7.0
 */
@Configuration
@EnableSwagger2
@SuppressWarnings("all")
public class SwaggerConfiguration {

	private static final Logger log = LoggerFactory.getLogger(SwaggerConfiguration.class);

	/**
	 * 어플리케이션 정보 Property.
	 */
	@Autowired
	protected AppInfoProperty appInfoProperty;

	/**
	 * Response 메시지 Property.
	 */
	@Autowired
	protected ResponseStatusProperty responseStatusProperty;

	@Bean
	public Docket swaggerApi() {
		log.debug("Swagger API...");
		List<ResponseMessage> responseMessageList = getResponseMessage();
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(getApiInfo()) // 기본설정.
				.useDefaultResponseMessages(false) //
				.globalResponseMessage(RequestMethod.GET, responseMessageList) // GET 메소드 에러 메시지 설정.
				.globalResponseMessage(RequestMethod.POST, responseMessageList) // POST 메소드 에러 메시지 설정.
				.globalResponseMessage(RequestMethod.PUT, responseMessageList) // PUT 메소드 에러 메시지 설정.
				.globalResponseMessage(RequestMethod.DELETE, responseMessageList) // DELETE 메소드 에러 메시지 설정.
				.select().apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
				.paths(PathSelectors.any()).build();
	}

	private ApiInfo getApiInfo() {
		return new ApiInfoBuilder().title(appInfoProperty.getDescription()) // 타이틀 정의
				.description(appInfoProperty.getDescription() + " with Swagger") // 설명 정의
				.termsOfServiceUrl("http://kthcorp.com") // 서비스 URL 정의
				.license("Apache License Version 2.0") // 라이센스 정의
				.licenseUrl("https://www.apache.org/licenses/LICENSE-2.0") // 라이센스 URL
				.version(appInfoProperty.getVersion()) // API 버전.
				.build();
	}

	private List<ResponseMessage> getResponseMessage() {
		List<ResponseMessage> messageList = Lists.newLinkedList();
		Map<Integer, String> messageMap = responseStatusProperty.getResult();
		Integer[] keys = messageMap.keySet().toArray(new Integer[0]);
		Arrays.sort(keys, new Comparator<Integer>() {
			@Override
			public int compare(Integer x, Integer y) {
				return x - y;
			}
		});
		for (Integer key : keys) {
			String message = messageMap.get(key);
			log.debug("Result Code: {}, Message: {}", key, message);
			messageList.add(new ResponseMessageBuilder() //
					.code(key) // Result Code
					.message(message) // Result Message
					.build());
		}
		return messageList;
	}

}
