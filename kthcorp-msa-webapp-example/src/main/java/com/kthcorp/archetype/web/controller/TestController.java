/*
 * Copyright(C) 2017 KT Hitel Co., Ltd. all rights reserved.
 *
 * This is a proprietary software of KTH corp, and you may not use this file except in
 * compliance with license with license agreement with KTH corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of KTH corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 */

package com.kthcorp.archetype.web.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.kthcorp.archetype.commons.dto.response.DefaultResponse;
import com.kthcorp.archetype.commons.web.controller.AbstractController;
import com.kthcorp.archetype.commons.web.util.ResponseUtils;

/**
 * Spring MVC Test Controller
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @version 1.0.0
 * @see AbstractController
 * @since 7.0
 */
@RestController
public class TestController extends AbstractController {

	private static final Logger log = LoggerFactory.getLogger(TestController.class);

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private DiscoveryClient discoveryClient;

	@RequestMapping("/service-instances/{applicationName}")
	public List<ServiceInstance> serviceInstancesByApplicationName(@PathVariable String applicationName) {
		return this.discoveryClient.getInstances(applicationName);
	}

	/**
	 * GET
	 *
	 * @param request HttpServletRequest
	 * @param response HttpServletResponse
	 * @return Object
	 */
	@RequestMapping("/test/get")
	public ResponseEntity<Object> get(HttpServletRequest request, HttpServletResponse response) {
		return ResponseUtils.resultJson(request, new DefaultResponse());
	}

	@RequestMapping("/test")
	public ResponseEntity<String> test(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String uri = "http://" + getAppInfo().getArtifact() + "/test/get";
		log.debug(">>>>> URI: {}", uri);
		return restTemplate.getForEntity(uri, String.class);
	}

}
