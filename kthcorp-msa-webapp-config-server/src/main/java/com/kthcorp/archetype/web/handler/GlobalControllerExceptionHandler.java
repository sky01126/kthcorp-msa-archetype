/*
 * Copyright(C) 2017 KT Hitel Co., Ltd. all rights reserved.
 *
 * This is a proprietary software of KTH corp, and you may not use this file except in
 * compliance with license with license agreement with KTH corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of KTH corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 */

package com.kthcorp.archetype.web.handler;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.kthcorp.archetype.commons.dto.response.DefaultResponse;
import com.kthcorp.archetype.commons.web.handler.AbstractExceptionHandler;
import com.kthcorp.archetype.commons.web.util.ResponseUtils;

/**
 * Global Controller 예외 처리.
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @version 1.0.0
 * @see AbstractExceptionHandler
 * @since 7.0
 */
@ControllerAdvice
public class GlobalControllerExceptionHandler extends AbstractExceptionHandler {

	private static final Logger log = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);

	/**
	 * HTTP Status 400 에러 처리.
	 *
	 * @param request HttpServletRequest
	 * @param error Throwable
	 * @return Object (ResponseEntity or ModelAndView)
	 */
	@Override
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	protected Object handleBadRequestException(final HttpServletRequest request, final Throwable error) {
		log.error("{} (URL={}, METHOD={})", error.getMessage(), request.getRequestURL(), request.getMethod());
		DefaultResponse res = new DefaultResponse(HttpStatus.BAD_REQUEST.value(),
				responseStatusProperty.get(HttpStatus.BAD_REQUEST.value()));
		return ResponseUtils.resultJson(request, res, HttpStatus.BAD_REQUEST);
	}

	/**
	 * HTTP Status 404 에러 처리.
	 *
	 * @param request HttpServletRequest
	 * @param error NoHandlerFoundException
	 * @return Object (ResponseEntity or ModelAndView)
	 */
	@Override
	@ResponseStatus(HttpStatus.NOT_FOUND)
	protected Object handleNotFoundException(final HttpServletRequest request, final NoHandlerFoundException error) {
		log.error("{} (URL={}, METHOD={})", error.getMessage(), request.getRequestURL(), request.getMethod());
		DefaultResponse res = new DefaultResponse(HttpStatus.NOT_FOUND.value(),
				responseStatusProperty.get(HttpStatus.NOT_FOUND.value()));
		return ResponseUtils.resultJson(request, res, HttpStatus.NOT_FOUND);
	}

	/**
	 * HTTP Status 405 에러 처리.
	 *
	 * @param request HttpServletRequest
	 * @param error HttpRequestMethodNotSupportedException
	 * @return Object (ResponseEntity or ModelAndView)
	 */
	@Override
	@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
	protected Object handleNethodNotAllowedException(final HttpServletRequest request,
			final HttpRequestMethodNotSupportedException error) {
		log.error("{} (URL={}, METHOD={})", error.getMessage(), request.getRequestURL(), request.getMethod());
		DefaultResponse res = new DefaultResponse(HttpStatus.METHOD_NOT_ALLOWED.value(),
				responseStatusProperty.get(HttpStatus.METHOD_NOT_ALLOWED.value()));
		return ResponseUtils.resultJson(request, res, HttpStatus.METHOD_NOT_ALLOWED);
	}

	/**
	 * Internal Server Error
	 *
	 * @param request HttpServletRequest
	 * @param error Throwable
	 * @return Object (ResponseEntity or ModelAndView)
	 */
	@Override
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	protected Object handleException(final HttpServletRequest request, final Throwable error) {
		log.error(error.getMessage(), error);
		DefaultResponse res = new DefaultResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
				responseStatusProperty.get(HttpStatus.INTERNAL_SERVER_ERROR.value()));
		return ResponseUtils.resultJson(request, res, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@Override
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	protected Object handleRuntimeException(HttpServletRequest request, RuntimeException ex) {
		if (log.isErrorEnabled()) {
			log.error(HttpStatus.INTERNAL_SERVER_ERROR.name(), ex);
		}
		DefaultResponse res = new DefaultResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(),
				responseStatusProperty.get(HttpStatus.INTERNAL_SERVER_ERROR.value()));
		return ResponseUtils.resultJson(request, res, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Parameter Bind Errror.
	 *
	 * @param request HttpServletRequest
	 * @param ex BindException
	 * @return Object (ResponseEntity or ModelAndView)
	 */
	@ExceptionHandler(BindException.class)
	@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
	protected Object handleBindException(HttpServletRequest request, BindException ex) {
		if (log.isErrorEnabled()) {
			log.error(ex.getMessage());
		}
		DefaultResponse res = new DefaultResponse(HttpStatus.PRECONDITION_FAILED.value(),
				responseStatusProperty.get(HttpStatus.PRECONDITION_FAILED.value()));
		return ResponseUtils.resultJson(request, res, HttpStatus.PRECONDITION_FAILED);
	}

	/**
	 * 지원하지 않는 미디어 타입 에러.
	 *
	 * @param request HttpServletRequest
	 * @param ex HttpMediaTypeNotSupportedException
	 * @return ResponseEntity
	 */
	@ExceptionHandler(HttpMediaTypeNotSupportedException.class)
	protected ResponseEntity<Object> handleHttpMediaTypeNotSupportedException(HttpServletRequest request,
			HttpMediaTypeNotSupportedException ex) {
		if (log.isErrorEnabled()) {
			log.error(ex.getMessage(), ex);
		}
		DefaultResponse res = new DefaultResponse(HttpStatus.NOT_ACCEPTABLE.value(),
				responseStatusProperty.get(HttpStatus.NOT_ACCEPTABLE.value()));
		return ResponseUtils.resultJson(request, res, HttpStatus.NOT_ACCEPTABLE);
	}

}
