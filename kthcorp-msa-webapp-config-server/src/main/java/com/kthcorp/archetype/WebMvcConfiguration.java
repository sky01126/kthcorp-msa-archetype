/*
 * Copyright(C) 2017 KT Hitel Co., Ltd. all rights reserved.
 *
 * This is a proprietary software of KTH corp, and you may not use this file except in
 * compliance with license with license agreement with KTH corp. Any redistribution or use of this
 * software, with or without modification shall be strictly prohibited without prior written
 * approval of KTH corp, and the copyright notice above does not evidence any actual or
 * intended publication of such software.
 */

package com.kthcorp.archetype;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

import com.kthcorp.archetype.commons.AbstractWebMvcConfiguration;
import com.kthcorp.commons.properties.AppInfoProperty;
import com.kthcorp.commons.web.annotation.RequestParamNameServletModelAttributeMethodProcessor;

/**
 * Web Mvc Configuration.
 *
 * @author <a href="mailto:ky.son@kt.com"><b>손근양</b></a>
 * @version 1.0.0
 * @see AbstractWebMvcConfiguration
 * @since 7.0
 */
@EnableWebMvc
@ComponentScan
@Configuration
@SuppressWarnings("all")
public class WebMvcConfiguration extends AbstractWebMvcConfiguration {

	private static final Logger log = LoggerFactory.getLogger(WebMvcConfiguration.class);

	/**
	 * 어플리케이션 정보 Property.
	 */
	@Autowired
	private AppInfoProperty appInfoProperty;

	/**
	 * Add resolvers to support custom controller method argument types.
	 *
	 * @param argumentResolvers initially an empty list
	 */
	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		log.debug("Add Argument Resolvers...");
		argumentResolvers.add(new RequestParamNameServletModelAttributeMethodProcessor());
	}

	/**
	 * Add handlers to serve static resources such as images, js, and, css
	 * files from specific locations under web application root, the classpath,
	 * and others.
	 *
	 * @param registry InterceptorRegistry
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		log.debug("Add Interceptors...");
		// ignore...
	}

	/**
	 * Add handlers to serve static resources such as images, js, and, css
	 * files from specific locations under web application root, the classpath,
	 * and others.
	 *
	 * @param registry ResourceHandlerRegistry
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		super.addResourceHandlers(registry);
		log.debug("Add Resource Handlers...");

		// Cross Domain 등록.
		registry.addResourceHandler("/crossdomain.xml") // Resource Handler
				.addResourceLocations("/crossdomain.xml"); // Resource Locations
		// .setCachePeriod(31556926); // Cache 기간 설정
	}

}
