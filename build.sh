#!/bin/bash

# Exit on error
set -e

VERSION='1.0.8.RELEASE'

## OS를 확인한다.
export OS='unknown'
if [[ "$(uname)" == "Darwin" ]]; then
    OS="darwin"
elif [[ "$(expr substr $(uname -s) 1 5)" == "Linux" ]]; then
    OS="linux"
fi

# Archetype Project Create
mvn clean
mvn -X archetype:create-from-project -DpackageName=com.kthcorp.archetype -Darchetype.properties=archetype.properties

# .gitignore 파일 복사.
cp .gitignore target/generated-sources/archetype/target/classes/archetype-resources/

# Archetype 프로젝트를 로컬 Repository에 등록
pushd target/generated-sources/archetype > /dev/null

# 임의로 kthcorp-msa를 ${parentArtifactId}로 변경한다.
sed -i "s/kthcorp-msa/\${parentArtifactId}/g" target/classes/archetype-resources/__rootArtifactId__-commons/pom.xml
sed -i "s/kthcorp-msa/\${parentArtifactId}/g" target/classes/archetype-resources/__rootArtifactId__-webapp-admin-server/pom.xml
sed -i "s/kthcorp-msa/\${parentArtifactId}/g" target/classes/archetype-resources/__rootArtifactId__-webapp-config-server/pom.xml
sed -i "s/kthcorp-msa/\${parentArtifactId}/g" target/classes/archetype-resources/__rootArtifactId__-webapp-eureka-server/pom.xml
sed -i "s/kthcorp-msa/\${parentArtifactId}/g" target/classes/archetype-resources/__rootArtifactId__-webapp-example/pom.xml

mvn install

## Nexus에 배포.
#mvn clean deploy -DaltDeploymentRepository=kthcorp-nexus-repo::default::https://nexus.kthcorp.com/content/repositories/releases/
#echo
#echo
#printf '\033[1m-------------------------------------------------------------------------------'
#echo
#sleep 0.5
#mvn clean
#
#if [ "$OS" == "darwin" ]; then # Mac OS
#    /usr/bin/open -a "/Applications/Google Chrome.app" "https://nexus.kthcorp.com/content/repositories/releases/com/kthcorp/archetype/kthcorp-msa-archetype/${VERSION}/"
#fi
