#!/bin/bash

# Exit on error
set -e

## OS를 확인한다.
export OS='unknown'
if [[ "$(uname)" == "Darwin" ]]; then
    OS="darwin"
elif [[ "$(expr substr $(uname -s) 1 5)" == "Linux" ]]; then
    OS="linux"
fi

# TODO 테스트 URL
SWAGGER_INPUT="http://localhost:8080/v2/api-docs"

# Swagger API URL을 입력받는다.
printf "Swagger API URL을 입력해주세요. ex. \e[00;32mhttp://localhost:8080/v2/api-docs\e[00m\n"
while [[ -z ${SWAGGER_INPUT} ]]; do
    read -e -p 'Enter swager api url> ' SWAGGER_INPUT
done

rm -rf ./doc

# Archetype Project Create
mvn clean
mvn swagger2markup:convertSwagger2markup process-resources -Dswagger.input=${SWAGGER_INPUT}

# 빌드 완료된 문서를 doc 디렉토리로 복사한다.
mkdir ./doc
mv ./target/asciidoc/html/*.html ./doc/
#mv ./target/asciidoc/pdf/*.pdf   ./doc/

# 빌드 디렉토리 삭제.
rm -rf ./target
