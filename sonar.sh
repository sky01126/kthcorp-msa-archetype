#!/bin/bash

# Exit on error
set -e

## OS를 확인한다.
export OS='unknown'
if [[ "$(uname)" == "Darwin" ]]; then
    OS="darwin"
elif [[ "$(expr substr $(uname -s) 1 5)" == "Linux" ]]; then
    OS="linux"
fi

export MAVEN_OPTS="-Xmx512m"

SONAR_HOST_URL='https://sonarqube.kthcorp.com'
SONAR_LOGIN='5ed63f95df8b6e75690eb3a520286ac0b84e8525'

mvn clean install

#mvn sonar:sonar                                                         \
#mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.2:sonar          \
mvn org.sonarsource.scanner.maven:sonar-maven-plugin:3.3.0.603:sonar    \
    -Dsonar.host.url=${SONAR_HOST_URL}                                  \
    -Dsonar.login=${SONAR_LOGIN}

mvn clean

if [ "$OS" == "darwin" ]; then # Mac OS
    /usr/bin/open -a "/Applications/Google Chrome.app" "${SONAR_HOST_URL}/dashboard?id=com.kthcorp.archetype%3Akthcorp-msa"
fi
